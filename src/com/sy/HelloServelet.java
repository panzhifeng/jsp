package com.sy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * @Author Kevin Durant
 * @create 2020/5/29 14:05
 */

public class HelloServelet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      //  super.doGet(req, resp);
        /*向浏览器输出8行，奇数行为红色，偶数行绿色
        * */


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doPost(req, resp);
        req.setCharacterEncoding("utf-8");

        resp.setContentType("text/html;charset=utf-8");

//        PrintWriter writer = resp.getWriter();
//
//            writer.println("<h1 style='color:green'>潘志峰</h1>");
//            writer.println("<h1 style='color:red'>年龄:29</h1>");
//            writer.println("<h1 style='color:green'>籍贯:江苏苏州</h1>");
//            System.out.println(req.getRemoteAddr());
        PrintWriter writer = resp.getWriter();
        String name = req.getParameter("text");
        String sel = req.getParameter("sel");
        String sex = req.getParameter("sex");
        String[] likes = req.getParameterValues("like");

        System.out.println(name);
        System.out.println(sel);
        System.out.println(sex);

        writer.print(" <head> <link rel='Stylesheet' href='layui/css/layui.css' type='text/css'> </head>");

        writer.println("<fieldset class=\"layui-elem-field layui-field-title\" style=\"margin-top: 20px;\">\n" +
                "  <legend>个人信息</legend>\n" +
                "</fieldset>\n" +
                " \n" +
                "<div class=\"layui-form\">\n" +
                "  <table class=\"layui-table\">\n" +
                "    <colgroup>\n" +
                "      <col width=\"150\">\n" +
                "      <col width=\"150\">\n" +
                "      <col width=\"200\">\n" +
                "      <col>\n" +
                "    </colgroup>\n" +
                "    <thead>\n" +
                "      <tr>\n" +
                "        <th>人物</th>\n" +
                "        <th>民族</th>\n" +
                "        <th>出场时间</th>\n" +
                "        <th>格言</th>\n" +
                "      </tr> \n" +
                "    </thead>\n" +
                "    <tbody>\n" +
                "      <tr>\n" +
                "        <td>"+name+"</td>\n" +
                "        <td>"+sel+"</td>\n" +
                "        <td>"+sex+"</td>\n" +
                "        <td>"+Arrays.toString(likes)+"</td>\n" +
                "      </tr>\n" +

                "    </tbody>\n" +
                "  </table>\n" +
                "</div>");

    }
}
